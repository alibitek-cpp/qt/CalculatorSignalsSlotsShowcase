#include "mainwindow.h"
#include "calculator.h"
#include <QApplication>
#include <QThread>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow window;
    window.show();

    Calculator calculator;

    QThread worker;
    worker.start();

    calculator.moveToThread(&worker);

    QObject::connect(&app, &QGuiApplication::lastWindowClosed, [&worker]() {
        worker.quit();
        worker.wait();
    });

    QObject::connect(&window, SIGNAL(evaluationRequested(const QString&)), &calculator, SLOT(evaluate(const QString&)));
    QObject::connect(&calculator, SIGNAL(evaluationCompleted(const QString&)), &window, SLOT(displayResult(const QString&)));

    return app.exec();
}
