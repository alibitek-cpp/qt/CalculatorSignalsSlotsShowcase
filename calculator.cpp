#include "calculator.h"
#include <QJSEngine>

Calculator::Calculator(QObject *parent) : QObject(parent)
{

}

void Calculator::evaluate(const QString &expression)
{
    QJSEngine engine;
    auto result = engine.evaluate(expression).toString();
    emit evaluationCompleted(result);
}
