#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void evaluationRequested(const QString& expression);

public slots:
    void displayResult(const QString& result);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
