#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QObject::connect(ui->inputField, &QLineEdit::editingFinished, this, [=]() {
        QString expression = ui->inputField->text();
        emit evaluationRequested(expression);
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::displayResult(const QString &result)
{
    ui->resultDisplay->setText("Result: " + result);
}
