#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QObject>

class Calculator : public QObject
{
    Q_OBJECT
public:
    explicit Calculator(QObject *parent = nullptr);

signals:
    void evaluationCompleted(const QString& result);

public slots:
    void evaluate(const QString& expression);
};

#endif // CALCULATOR_H
